// archivo que va a contener todas las rutaas publicas del proyecto
const express = require("express");
const router = express.Router();
const {getAll} = require("../db/conexion");
//const db = require("../db/conexion");
require("dotenv").config();
// Las Rutas de mi proyecto


// Función para mostrar solo el primer nombre de los integrantes
function obtenerPrimerNombre(integrantes) {
    return integrantes.map(integrante => {
        const primerNombre = integrante.nombre.split(" ")[0];
        return {...integrante, nombre: primerNombre};
    })
}

//......................Direcciones.............
router.use((req, res, next) => {
    res.locals.ENLACE=process.env.ENLACE;
    res.locals.NOMBRE=process.env.NOMBRE;
    res.locals.APELLIDO=process.env.APELLIDO;    
    res.locals.MATERIA=process.env.MATERIA;
    next();
});

//ruta localhost:{puerto}
router.get("/", async(request,response) => {
    //const sql = "SELECT * FROM integrantes WHERE activo = 1";
    const sql = await getAll("select * from integrantes where activo = 1 order by orden");
    const primerNombreIntegrantes = obtenerPrimerNombre(sql);

    try {
        //const rows = await getAll(sql); // Asegúrate de que getAll esté correctamente implementado
        
        response.render("index", {
            integrantes: primerNombreIntegrantes,
            showAdminLink: true, // Se habilita Admin solo dentro de la página de inicio
        });
    } catch (err) {
        console.error(err.message);
        response.status(500).send("Error Interno del Servidor");
    }
});

//cuando escriba la direccion http://localhost:3000/paginas/wordcloud/index.html
router.get("/wordcloud", async(request,response) => {
    const rows = await getAll("select * from integrantes")
    response.render("wordcloud",{
        integrantes: rows,
    })
});

//cuando escriba la direccion http://localhost:3000/paginas/info_curso/index.html
router.get("/info_curso", async(request,response) => {
    const rows = await getAll("select * from integrantes")
    response.render("info_curso",{
        integrantes: rows,
    })
});

// *****************************************************************
// Detalle por matrícula
router.get("/:matricula", async (req, res, next) => {
    const matriculas = (await getAll("SELECT matricula FROM integrantes WHERE activo = 1 ORDER BY orden")).map(obj => obj.matricula);
    const tipoMedia = await getAll("SELECT * FROM tipoMedia WHERE activo = 1 ORDER BY orden");
    const matricula = req.params.matricula;
    const integrantes = await getAll("SELECT * FROM integrantes WHERE activo = 1 ORDER BY orden");
    const primerNombreIntegrantes = obtenerPrimerNombre(integrantes);

    if (matriculas.includes(matricula)) {
        const integranteFilter = await getAll("SELECT * FROM integrantes WHERE activo = 1 AND matricula = ? ORDER BY orden", [matricula]);
        const mediaFilter = await getAll("SELECT * FROM media WHERE activo = 1 AND matricula = ? ORDER BY orden", [matricula]);

        res.render('integrantes', {
            integrante: integranteFilter,
            tipoMedia: tipoMedia,
            media: mediaFilter,
            integrantes: primerNombreIntegrantes
        });
    } else {
        next();
    }
});

//exportamos al router
module.exports = router;
