const express = require("express");
const router = express.Router();
const {getAll, run, getLastOrder, matriculaExistente, getLastId, obtenerTipoMedia} = require("../db/conexion");
const fs = require('fs').promises;
const multer = require('multer');
const upload = multer({ dest: "./public/images"});
//const fileUpload = upload.single('scr');
const uploadFields = upload.fields([
    { name: 'src_representativa', maxCount: 1 },
    { name: 'src_propia', maxCount: 1 }
]);

const AdminIntegrantesController = require('../controllers/admin/integrantes.controller');
const AdminTipoMediaController = require('../controllers/admin/tipoMedia.controller');
const AdminMediaController = require('../controllers/admin/media.controller');
//Como prefijo /admin
router.get("/", (req, res) => {
    res.render("admin/index");
});

//INTEGRANTES
//Como prefijo /admin/integrantes/listar
router.get("/integrantes/listar", AdminIntegrantesController.index);
router.get("/integrantes/crear", AdminIntegrantesController.create);
router.post("/integrantes/crear", AdminIntegrantesController.store);
router.post("/integrantes/eliminar/:matricula", AdminIntegrantesController.destroy);


// TIPO MEDIA
//Como prefijo /admin/tipo_media/listar
router.get("/tipo_media/listar", AdminTipoMediaController.index);
router.get("/tipo_media/crear", AdminTipoMediaController.create);
router.post("/tipo_media/crear", AdminTipoMediaController.store);


// MEDIA
//Como prefijo /admin/media/listar
router.get("/media/listar", AdminMediaController.index);
router.get("/media/crear", AdminMediaController.create);
router.post("/media/crear", AdminMediaController.store);


//exportar
module.exports = router;