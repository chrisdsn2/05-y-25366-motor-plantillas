CREATE TABLE IF NOT EXISTS "integrantes" (
	"matricula" TEXT NOT NULL UNIQUE,
	"nombre" TEXT NOT NULL,
	"apellido" TEXT NOT NULL,
	"codigo" INTEGER NOT NULL UNIQUE,
	"orden" INTEGER NOT NULL,
	"activo" INTEGER NOT NULL DEFAULT 1,
	PRIMARY KEY("matricula")
	
);

CREATE TABLE IF NOT EXISTS "media" (
	"id" INTEGER NOT NULL UNIQUE,
	"matricula" TEXT NOT NULL,
	"titulo" TEXT NOT NULL,
	"video" TEXT NOT NULL,
	"scr_propia" TEXT NOT NULL,
	"scr" TEXT NOT NULL,
	"id_tmedia" INTEGER NOT NULL,
	"orden" INTEGER NOT NULL,
	"activo" INTEGER NOT NULL DEFAULT 1,
	PRIMARY KEY("id"),
             FOREIGN KEY ("matricula") REFERENCES "integrantes"("matricula")
             FOREIGN KEY ("id_tmedia") REFERENCES "tipoMedia"("id")
	
         
);

CREATE TABLE IF NOT EXISTS "tipoMedia" (
	"id" INTEGER NOT NULL UNIQUE,
	"nombre" TEXT NOT NULL UNIQUE,
	"orden" INTEGER NOT NULL,
	"activo" INTEGER NOT NULL DEFAULT 1,
	PRIMARY KEY("id")
	
);