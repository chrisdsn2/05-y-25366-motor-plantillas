const sqlite3 = require("sqlite3").verbose();

const db = new sqlite3.Database(
    "./db/integrantes.sqlite3",// con el db/ busca desde la carpeta raiz./db/integrantes.sqlite3
    sqlite3.OPEN_READWRITE,//que abra el archivo lectura escritura y si no pasa al error
    (error)=>{
        if(error) console.log("Ocurrio un error", error.message);
        else{
            console.log("Conexion Exitosa");
           // db.run("select * from integrantes")// Para verificar que estamos en la bd correcta
        }
    }
);

// Función para obtener todos los registros de una consulta
async function getAll(query, params) {
    return new Promise((resolve, reject) => {
        db.all(query, params, (error, rows) => {
            if (error) {
                reject(error);
            } else {
                resolve(rows);
            }
        });
    });
}

// Función para obtener el último orden de una tabla
async function getLastOrder(tabla) {
        const result = await getAll(`SELECT MAX(orden) as maxOrder FROM ${tabla}`);
        return result[0].maxOrder;    
};

// Función para ejecutar una consulta de modificación (insert, update, delete)
async function run(query, params) {
    return new Promise((resolve, reject) => {
        db.run(query, params, function (error) {
            if (error) {
                reject(error);
            } else {
                resolve();
            }
        })
    })
};

async function getOne(sql, params = []) {
    return new Promise((resolve, reject) => {
        db.get(sql, params, (err, row) => {
            if (err) {
                reject(err);
            } else {
                resolve(row);
            }
        });
    });
};

// Función para verificar si una matrícula ya existe
async function matriculaExistente(matricula) {
    const result = await getAll("SELECT 1 FROM integrantes WHERE matricula = ?", [matricula]);
    return result.length > 0;
};

async function getLastId(tablaNombre) {
    const result = await getAll(`SELECT MAX(id) as maxId FROM ${tablaNombre}`);
    return result[0].maxId;
};
const obtenerTipoMedia = async () => {
    const sql = "SELECT * FROM tipoMedia WHERE activo = 1";
    try {
      const tipoMedia = await getAll(sql);
      return tipoMedia;
    } catch (error) {
      console.error(error);
      return [];
    }
  };

module.exports = {
    db,
    getAll,
    run,
    getLastOrder,
    getOne,
    matriculaExistente,
    getLastId,
    obtenerTipoMedia
};


//module.exports = {db,getAll};