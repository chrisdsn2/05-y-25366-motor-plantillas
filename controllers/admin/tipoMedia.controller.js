const { create } = require("hbs");
const {getAll, run, getLastOrder, matriculaExistente, getLastId, obtenerTipoMedia} = require("../../db/conexion");

const TipoMediaController = {
    index: async function (req, res) {
        const sqltm = "SELECT * FROM tipoMedia WHERE activo = 1";
        console.log("Buscando Tipo Media...");// Si hay error muestra en consola

        try {
            const tipoMedia = await getAll(sqltm);
        console.log("Tipo Media obtenido con éxito:", tipoMedia); // Si hay error muestra en consola

            res.render("admin/tipo_media/index", {
                tipoMedia: tipoMedia,
            });
        } catch (err) {
            console.error("Error buscando Tipo Media:", err.message);
            res.status(500).send("Error Interno del Servidor");
        }
    },

    create: async function (req, res) {
        res.render('admin/tipo_media/crearTipoMedia',{
            nombre: req.query.nombre || '',
            activo: req.query.activo || '1'  // Valor por defecto 'activo'
        });
    },
    store: async function (req, res) {
        let errores = [];
        console.log("Verificando Tipo Media...");
        const lastOrder = await getLastOrder('tipoMedia');
        const newOrder = lastOrder + 1;


        if (!req.body.nombre || req.body.nombre.trim() === '') {
            errores.push('¡El nombre no puede estar vacío!');
        }

        if (req.body.activo === undefined) {
            errores.push('¡Debe seleccionar un estado (activo o inactivo)!');
        }

        if (errores.length > 0) {
            console.log("Errores encontrados:", errores);
            res.redirect(`/admin/tipo_media/crear?error=${encodeURIComponent(errores.join(';'))}
            &nombre=${encodeURIComponent(req.body.nombre || '')}`);
            return;  // Detiene la ejecución del código si hay errores
        }

        try {
            console.log("Insertando nuevo tipo de media...");
            await run("INSERT INTO tipoMedia (id, nombre, orden, activo) VALUES (?, ?, ?, ?)",
                [
                    req.body.id,
                    req.body.nombre,
                    newOrder,
                    req.body.activo
                ]);
            console.log("Redirigiendo...");
            res.redirect(`/admin/tipo_media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
        } catch (err) {
            console.error('Error al insertar el tipo de media:', err);
            res.redirect(`/admin/tipo_media/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
        }
    },


}

module.exports = TipoMediaController;