const { create } = require("hbs");
//const {getAll} = require("../../db/conexion");
const {getAll, run, getLastOrder, matriculaExistente, getOne} = require("../../db/conexion");
/*index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro*/


//index - listado /admin/integrantes/listar
const IntegrantesController = {
    index: async function (req, res) {
        const sql = "SELECT * FROM integrantes WHERE activo = 1";
        console.log("Buscando integrantes...");// Si hay error muestra en consola
        try {
            const integrantes = await getAll(sql);
        // console.log("Integrantes obtenido con éxito:", integrantes); // Si hay error muestra en consola

            res.render("admin/integrantes/index", {
                integrantes: integrantes,
               // message: req.query.message
            });
        } catch (err) {
            console.error("Error fetching integrantes:", err.message);
            res.status(500).send("Error Interno del Servidor");
        }
    },
//create - formulario de creación
    create: function (req, res) {
        res.render('admin/integrantes/crearFormIntegrantes', {
            matricula: req.query.matricula || '',
            nombre: req.query.nombre || '',
            apellido: req.query.apellido || ''
        });
    },
//store - método de guardar en la base de datos
    store: async function (req, res) {
        let errores = [];

        console.log("Verificando matrícula...");
        const lastOrder = await getLastOrder('integrantes');
        const newOrder = lastOrder + 1;
    
        if (!req.body.matricula || req.body.matricula.trim() === '') {
            errores.push('¡La matrícula no puede estar vacía!');
        }
    
        if (!req.body.nombre || req.body.nombre.trim() === '' || req.body.nombre.length > 50) {
            errores.push('¡El nombre no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }
    
        if (!req.body.apellido || req.body.apellido.trim() === '' || req.body.apellido.length > 50) {
            errores.push('¡El apellido no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }
    
        if (!req.body.codigo || req.body.codigo.trim() === '' || req.body.codigo.length > 10) {
            errores.push('¡El codigo no puede estar vacío y debe tener una longitud máxima de 10 caracteres!');
        }
    
        if (req.body.activo === undefined) {
            errores.push('¡Debe seleccionar un estado (activo o inactivo)!');
        }
    
        if (errores.length > 0) {
            console.log("Errores encontrados:", errores);
            res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent(errores.join(';'))}           
            &matricula=${encodeURIComponent(req.body.matricula || '')}
            &nombre=${encodeURIComponent(req.body.nombre || '')}
            &apellido=${encodeURIComponent(req.body.apellido || '')}
            &codigo=${encodeURIComponent(req.body.codigo || '')}`);
            return;  // Detiene la ejecución del código si hay errores
        }
    
        console.log("Verificando si la matrícula existe...");
        try {
            if (await matriculaExistente(req.body.matricula)) {
                errores.push('¡La matrícula ya existe!');
                console.log("Errores encontrados:", errores);
                res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent(errores.join(';'))}
                &matricula=${encodeURIComponent(req.body.matricula)}
                &nombre=${encodeURIComponent(req.body.nombre)}
                &apellido=${encodeURIComponent(req.body.apellido)}
                &codigo=${encodeURIComponent(req.body.codigo)}`);
                return;  // Detiene la ejecución del código si la matrícula ya existe
            }
        } catch (error) {
            console.error("Error al verificar si la matrícula existe:", error);
            res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent('¡Error al verificar la matrícula!')}`);
            return;  // Detiene la ejecución del código en caso de error
        }
    
        try {
            console.log("Insertando nuevo integrante...");
            await run("INSERT INTO integrantes (matricula, nombre, apellido, codigo, orden, activo) VALUES (?, ?, ?, ?, ?, ?)",
                [
                    req.body.matricula,
                    req.body.nombre,
                    req.body.apellido,
                    req.body.codigo,
                    newOrder,
                    req.body.activo
                ]);
                console.log("Redirigiendo...");
                res.redirect(`/admin/integrantes/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
            console.error('Error al insertar el integrante:', err);
            res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
        }
    },
    //show - formulario de ver un registor
    //update - método de editar un registro
    //edit - formulario de edición
    //destroy - operación de eliminar un registro
    destroy: async function (req, res) {
    const matricula = req.params.matricula;
    
    // Verificar si el registro está siendo usado en otra tabla FK
    const checkSql = "SELECT COUNT(*) AS count FROM media WHERE matricula = ?";
    try {
        const resultCheck = await getOne(checkSql, [matricula]);
        if (!resultCheck || resultCheck.count > 0) {
            res.redirect(`/admin/integrantes/listar?error=${encodeURIComponent('¡El registro está siendo utilizado en otra tabla!')}`);
            return;
        }

        // Realizar el borrado lógico
        const sql = "UPDATE integrantes SET activo = 0 WHERE matricula = ?";
        const resultUpdate = await run(sql, [matricula]);
        
        if (resultUpdate && resultUpdate.changes > 0) {
            res.redirect(`/admin/integrantes/listar?success=${encodeURIComponent('¡Integrante eliminado correctamente!')}`);
        } else {
            res.redirect(`/admin/integrantes/listar?error=${encodeURIComponent('¡Error al intentar eliminar el integrante!')}`);
        }
    } catch (err) {
        console.error("Error en borrado logico integrante:", err.message);
        res.status(500).send("Error Interno del Servidor");
    }
}
};




module.exports = IntegrantesController;