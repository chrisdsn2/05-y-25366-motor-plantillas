const {getAll, run, getLastOrder, matriculaExistente, getLastId, obtenerTipoMedia} = require("../../db/conexion");

const MediaController = {
    index: async function (req, res) {

        const sqlm = `
            SELECT media.id, media.matricula, media.titulo, media.video, media.scr_propia, media.scr, 
            tipoMedia.nombre AS nombre_tmedia, media.orden, media.activo
            FROM media
            JOIN tipoMedia ON media.id_tmedia = tipoMedia.id
            WHERE media.activo = 1
            `;
            console.log("Buscando Media...");// Si hay error muestra en consola

        try {
            const media = await getAll(sqlm);
        console.log("Tipo Media obtenido con éxito:", media); // Si hay error muestra en consola

            res.render("admin/media/index", {
                media: media,
            });
        } catch (err) {
            console.error("Error buscando media:", err.message);
            res.status(500).send("Error Interno del Servidor");
        }
    },

    create: async function (req, res) {
        try {
            const integrantes = await getAll("SELECT * FROM integrantes WHERE activo = 1 ORDER BY nombre");
            const tipoMedia = await getAll("SELECT * FROM tipoMedia WHERE activo = 1 ORDER BY nombre");
            res.render("admin/media/crearMedia", {
                integrantes: integrantes,
                tipoMedia: tipoMedia,
                tipoMediaSeleccionado: req.query.tipoMedia || "",
                url: req.query.url || "",
                titulo: req.query.titulo || "",
                alt: req.query.alt || "",
                integranteSeleccionado: req.query.integrante || ""
            });
        } catch (err) {
            console.error(err);
            res.status(500).send("Error al cargar la página de creación de media.");
        }
    },

    store: async function (req, res) {
        let errores = [];

        // Validaciones
        if (!req.body.tipoMedia) {
            errores.push('¡Debe seleccionar un tipo de media!');
        }
        if (!req.body.integrante) {
            errores.push('¡Debe seleccionar un integrante!');
        }
        if (req.body.url && (req.file || req.body.src_representativa || req.body.src_propia)) {
            errores.push('¡No puedes agregar tanto URL como SRC al mismo tiempo!');
        }
        if (!req.body.titulo || req.body.titulo.length > 50) {
            errores.push('¡El título no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
        }
    
        if (errores.length > 0) {
            res.redirect(`/admin/media/crear?error=${encodeURIComponent(errores.join(';'))}&tipoMedia=${encodeURIComponent(req.body.tipoMedia)}&url=${encodeURIComponent(req.body.url)}&titulo=${encodeURIComponent(req.body.titulo)}&alt=${encodeURIComponent(req.body.alt)}&integrante=${encodeURIComponent(req.body.integrante)}`);
        } else {
            const lastOrder = await getLastOrder('media');
            const newOrder = lastOrder + 1;
            const lastId = await getLastId('media');
            const newId = lastId + 1;
    
            let srcRepresentativaPath = '';
            let srcPropiaPath = '';
    
            if (req.files.src_representativa) {
                srcRepresentativaPath = `/images/${req.files.src_representativa[0].filename}`;
                console.log("srcRepresentativaPath:", srcRepresentativaPath);
            }
    
            if (req.files.src_propia) {
                srcPropiaPath = `/images/${req.files.src_propia[0].filename}`;
                console.log("srcPropiaPath:", srcPropiaPath);
            }
    
            try {
                await run("INSERT INTO media (id, matricula, titulo, video, scr_propia, scr, id_tmedia, orden, activo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                    [
                        newId,
                        req.body.integrante,
                        req.body.titulo,
                        req.body.url || "",
                        srcPropiaPath,
                        srcRepresentativaPath,
                        req.body.tipoMedia,
                        newOrder,
                        req.body.activo ? 1 : 1
                    ]
                );
                res.redirect(`/admin/media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.error(err);
                res.redirect(`/admin/media/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
    }
};

module.exports = MediaController;